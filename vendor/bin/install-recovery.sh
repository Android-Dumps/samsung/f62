#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery$(getprop ro.boot.slot_suffix):71102464:88857712f091900267ac7ac12b56b3c053fa3ffb; then
  applypatch \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot$(getprop ro.boot.slot_suffix):61865984:f88c802c283d0c7fdadaffe86b9170f198dae0ce \
          --target EMMC:/dev/block/by-name/recovery$(getprop ro.boot.slot_suffix):71102464:88857712f091900267ac7ac12b56b3c053fa3ffb && \
      (log -t install_recovery "Installing new recovery image: succeeded" && setprop vendor.ota.recovery.status 200) || \
      (log -t install_recovery "Installing new recovery image: failed" && setprop vendor.ota.recovery.status 454)
else
  log -t install_recovery "Recovery image already installed" && setprop vendor.ota.recovery.status 200
fi

