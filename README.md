## f62ins-user 12 SP1A.210812.016 E625FDDU2BVC3 release-keys
- Manufacturer: samsung
- Platform: universal9825-r
- Codename: f62
- Brand: samsung
- Flavor: f62ins-user
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: E625FDDU2BVC3
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: samsung/f62ins/f62:12/SP1A.210812.016/E625FDDU2BVC3:user/release-keys
- OTA version: 
- Branch: f62ins-user-12-SP1A.210812.016-E625FDDU2BVC3-release-keys
- Repo: samsung/f62
